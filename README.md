# mpe

My example Postgres extension

## Building

### Building on Ubuntu 22.04

Confirmed works on Postgres 10, 12 and 14

```bash
# install dependencies
sudo apt update

sudo apt install -y \
    git \
    make \
    cmake \
    postgresql-server-dev-all # Version 14 is confirmed compatible

git clone https://gitlab.com/mtyszczak/mpe.git
cd mpe
git checkout master

mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
make -j$(nproc)

sudo make install
```

This shall build all of the required extensions

## Example usage

```sql
CREATE EXTENSION "mpe";

SELECT * FROM
( VALUES ( '' :: mpe.type ) ) as alias1
  EXCEPT SELECT * FROM
    ( VALUES ( '' :: mpe.type ) ) as alias2;
```

Results in:

```txt
ERROR:  could not identify an equality operator for type mpe.type
LINE 1: SELECT * FROM
```

### Fixed in 85b3cbd1

## License

Distributed under the GNU GENERAL PUBLIC LICENSE Version 3
See [LICENSE.md](LICENSE.md)
