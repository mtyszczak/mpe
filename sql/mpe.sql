CREATE TYPE mpe.type;


CREATE OR REPLACE FUNCTION mpe.type_in(
  cstring
) RETURNS mpe.type LANGUAGE c IMMUTABLE STRICT PARALLEL SAFE
AS 'MODULE_PATHNAME',
'type_in';

CREATE OR REPLACE FUNCTION mpe.type_out(
  mpe.type
) RETURNS cstring LANGUAGE c IMMUTABLE STRICT PARALLEL SAFE
AS 'MODULE_PATHNAME',
'type_out';

CREATE OR REPLACE FUNCTION mpe.type_eq(
  mpe.type, mpe.type
) RETURNS bool LANGUAGE C IMMUTABLE STRICT PARALLEL SAFE
AS 'MODULE_PATHNAME',
'type_eq';

CREATE OR REPLACE FUNCTION mpe.type_ne(
  mpe.type, mpe.type
) RETURNS bool LANGUAGE C IMMUTABLE STRICT PARALLEL SAFE
AS 'MODULE_PATHNAME',
'type_ne';

CREATE OR REPLACE FUNCTION mpe.type_lt(
  mpe.type, mpe.type
) RETURNS bool LANGUAGE C IMMUTABLE STRICT PARALLEL SAFE
AS 'MODULE_PATHNAME',
'type_lt';

CREATE OR REPLACE FUNCTION mpe.type_le(
  mpe.type, mpe.type
) RETURNS bool LANGUAGE C IMMUTABLE STRICT PARALLEL SAFE
AS 'MODULE_PATHNAME',
'type_le';

CREATE OR REPLACE FUNCTION mpe.type_gt(
  mpe.type, mpe.type
) RETURNS bool LANGUAGE C IMMUTABLE STRICT PARALLEL SAFE
AS 'MODULE_PATHNAME',
'type_gt';

CREATE OR REPLACE FUNCTION mpe.type_ge(
  mpe.type, mpe.type
) RETURNS bool LANGUAGE C IMMUTABLE STRICT PARALLEL SAFE
AS 'MODULE_PATHNAME',
'type_ge';

CREATE OR REPLACE FUNCTION mpe.type_cmp(
  mpe.type, mpe.type
) RETURNS int4 LANGUAGE C IMMUTABLE STRICT PARALLEL SAFE
AS 'MODULE_PATHNAME',
'type_cmp';


CREATE TYPE mpe.type(
    INPUT = mpe.type_in
  , OUTPUT = mpe.type_out

  , INTERNALLENGTH = VARIABLE
  , STORAGE = extended

  , COLLATABLE     = true
);

CREATE OPERATOR < (
    LEFTARG    = mpe.type,
    RIGHTARG   = mpe.type,
    COMMUTATOR = <,
    NEGATOR    = >=,
    PROCEDURE  = mpe.type_lt,
        RESTRICT = contsel,
	JOIN = contjoinsel
);

CREATE OPERATOR <= (
    LEFTARG    = mpe.type,
    RIGHTARG   = mpe.type,
    COMMUTATOR = <=,
    NEGATOR    = >,
    PROCEDURE  = mpe.type_le,
        RESTRICT = contsel,
	JOIN = contjoinsel
);

CREATE OPERATOR > (
    LEFTARG    = mpe.type,
    RIGHTARG   = mpe.type,
    COMMUTATOR = >,
    NEGATOR    = <=,
    PROCEDURE  = mpe.type_gt,
        RESTRICT = contsel,
	JOIN = contjoinsel
);

CREATE OPERATOR >= (
    LEFTARG    = mpe.type,
    RIGHTARG   = mpe.type,
    COMMUTATOR = >=,
    NEGATOR    = <,
    PROCEDURE  = mpe.type_ge,
        RESTRICT = contsel,
	JOIN = contjoinsel
);

CREATE OPERATOR = (
    LEFTARG    = mpe.type,
    RIGHTARG   = mpe.type,
    COMMUTATOR = =,
    NEGATOR    = !=,
    PROCEDURE  = mpe.type_eq,
	RESTRICT = eqsel, JOIN = eqjoinsel,
	MERGES
);

CREATE OPERATOR != (
    LEFTARG    = mpe.type,
    RIGHTARG   = mpe.type,
    NEGATOR    = =,
    COMMUTATOR = !=,
    PROCEDURE  = mpe.type_ne,
	RESTRICT = neqsel, JOIN = neqjoinsel
);

CREATE OPERATOR CLASS type_ops
DEFAULT FOR TYPE mpe.type USING btree AS
    OPERATOR    1   <  (mpe.type, mpe.type),
    OPERATOR    2   <= (mpe.type, mpe.type),
    OPERATOR    3   =  (mpe.type, mpe.type),
    OPERATOR    4   >= (mpe.type, mpe.type),
    OPERATOR    5   >  (mpe.type, mpe.type),
    FUNCTION    1   mpe.type_cmp(mpe.type, mpe.type);
