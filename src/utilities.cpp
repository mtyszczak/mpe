extern "C"
{

#include <postgres.h>
#include <fmgr.h>

PG_MODULE_MAGIC;

typedef struct
{
  char vl_size_[VARHDRSZ];          /* varlena header (do not touch directly!) */
  char data[FLEXIBLE_ARRAY_MEMBER]; // raw type data
} _type;

#define PG_RETURN_MPE_TYPE( x ) PG_RETURN_POINTER( x )

#define DatumGetMPETypePP( X )     ( (_type*) PG_DETOAST_DATUM( X ) )
#define PG_GETARG_MPE_TYPE_PP( n ) DatumGetMPETypePP( PG_GETARG_DATUM( n ) )


  PG_FUNCTION_INFO_V1( type_eq );
  Datum type_eq( PG_FUNCTION_ARGS )
  { PG_RETURN_BOOL( true ); }

  PG_FUNCTION_INFO_V1( type_le );
  Datum type_le( PG_FUNCTION_ARGS )
  { PG_RETURN_BOOL( true ); }

  PG_FUNCTION_INFO_V1( type_ge );
  Datum type_ge( PG_FUNCTION_ARGS )
  { PG_RETURN_BOOL( true ); }

  PG_FUNCTION_INFO_V1( type_lt );
  Datum type_lt( PG_FUNCTION_ARGS )
  { PG_RETURN_BOOL( false ); }

  PG_FUNCTION_INFO_V1( type_gt );
  Datum type_gt( PG_FUNCTION_ARGS )
  { PG_RETURN_BOOL( false ); }

  PG_FUNCTION_INFO_V1( type_ne );
  Datum type_ne( PG_FUNCTION_ARGS )
  { PG_RETURN_BOOL( false ); }

  PG_FUNCTION_INFO_V1( type_cmp );
  Datum type_cmp( PG_FUNCTION_ARGS )
  { PG_RETURN_INT32( 0 ); }


  PG_FUNCTION_INFO_V1( type_in );
  Datum type_in( PG_FUNCTION_ARGS )
  {
    _type* t = (_type*) palloc( 0 + VARHDRSZ );
    SET_VARSIZE( t, 0 + VARHDRSZ );

    PG_RETURN_MPE_TYPE( t );
  }

  PG_FUNCTION_INFO_V1( type_out );
  Datum type_out( PG_FUNCTION_ARGS )
  {
    uint32 op_ccp_size = 0 + 1;
    char* op_ccp       = (char*) palloc( op_ccp_size );
    *op_ccp = '\0';

    PG_RETURN_CSTRING( op_ccp );
  }

}
